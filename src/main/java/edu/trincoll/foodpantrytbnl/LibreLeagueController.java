package edu.trincoll.foodpantrytbnl;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class LibreLeagueController {
    private final List<String> teamMembers =
            List.of("Josh","Hallie","Yusuke","Benedicte","Hamim");

    @GetMapping("/libreleague")
    public List<String> getTeamMembers() {
        return teamMembers;
    }
}

